package com.example.assignment_3_giorgos_and_hakon.models;

import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Franchise {
    //FIELDS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int id;
    @Column(length = 50,nullable = false)
    private String name;

    private String description;


    //Relationships
//Relations
    @OneToMany(mappedBy = "belongedFranchise")
    private Set<Movie> movies;


    //Override method for toString to get proper output
    @Override
    public String toString() {
        return "Franchise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ", movies=" + movies +
                '}';
    }

    //GETTERS AND SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
