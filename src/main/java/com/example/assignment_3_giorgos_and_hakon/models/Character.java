package com.example.assignment_3_giorgos_and_hakon.models;


import jakarta.persistence.*;

import java.util.Set;

@Entity
public class Character {

    //FIELDS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private int id;
    @Column(length = 70,nullable = false)
    private String name;

    @Column(length = 50)
    private String alias;
    @Column(length = 10,nullable = false)
    private String gender;

    @Column(length = 500)
    private String pictureUrl;



    //Relationships
    @ManyToMany
    @JoinTable(
            name="Character_Movies",
            joinColumns = {@JoinColumn(name="Character_id")},
            inverseJoinColumns = {@JoinColumn(name="Movie_id")}
    )
    Set<Movie> moviesPlayed;


    //Override toString for proper output
    @Override
    public String toString() {
        return "Character{" +
                "id=" + id +
                ", fullName='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", Gender='" + gender + '\'' +
                ", photo='" + pictureUrl + '\'' +
                ", moviesPlayed=" + moviesPlayed +
                '}';
    }

    //GETTERS AND SETTERS

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Set<Movie> getMoviesPlayed() {
        return moviesPlayed;
    }

    public void setMoviesPlayed(Set<Movie> moviesPlayed) {
        this.moviesPlayed = moviesPlayed;
    }
}
