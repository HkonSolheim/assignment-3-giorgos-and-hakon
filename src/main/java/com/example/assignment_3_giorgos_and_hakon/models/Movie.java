package com.example.assignment_3_giorgos_and_hakon.models;
import jakarta.persistence.*;
import java.util.Set;

@Entity
public class Movie {

    //FIELDS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (nullable = false)
    private int id;
    @Column(length = 50,nullable = false)
    private String title;

    @Column(length = 35,nullable = false)
    private String genre;

    @Column(length = 50)
    private int releaseYear;

    @Column(length = 60,nullable = false)
    private String director;

    private String pictureUrl;

    private String trailerLink;


    //Relationships
    @ManyToMany(mappedBy = "moviesPlayed")
    Set<Character> characters;
    @ManyToOne
    private Franchise belongedFranchise;



    //Override toString for proper output
    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", movieTitle='" + title + '\'' +
                ", Genre='" + genre + '\'' +
                ", ReleaseYear=" + releaseYear +
                ", Director='" + director + '\'' +
                ", Picture='" + pictureUrl + '\'' +
                ", trailer='" + trailerLink + '\'' +
                ", characters=" + characters +
                ", belongedFranchise=" + belongedFranchise +
                '}';
    }
    //GETTERS AND SETTERS
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getTrailerLink() {
        return trailerLink;
    }

    public void setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
    }

    public Set<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }

    public Franchise getBelongedFranchise() {
        return belongedFranchise;
    }

    public void setBelongedFranchise(Franchise belongedFranchise) {
        this.belongedFranchise = belongedFranchise;
    }
}

