package com.example.assignment_3_giorgos_and_hakon.runner;
import com.example.assignment_3_giorgos_and_hakon.models.Character;
import com.example.assignment_3_giorgos_and_hakon.repositories.CharacterRepository;
import jakarta.transaction.Transactional;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Component
public class PgRunner implements CommandLineRunner {



    private final CharacterRepository characterRepository;

    public PgRunner(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        //Character character = characterRepository.findById(1).get();
        //System.out.println(character);

    }
}
