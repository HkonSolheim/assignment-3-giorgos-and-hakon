package com.example.assignment_3_giorgos_and_hakon.repositories;

import com.example.assignment_3_giorgos_and_hakon.models.Character;
import org.springframework.boot.ApplicationArguments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

import static org.aspectj.weaver.UnresolvedType.add;

@Repository
public interface CharacterRepository extends JpaRepository<Character,Integer> {
    @Query("select c from Character c where c.name like ?1")
    Set<Character> findAllByName(String name);

}
