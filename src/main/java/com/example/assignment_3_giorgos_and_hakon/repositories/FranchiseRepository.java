package com.example.assignment_3_giorgos_and_hakon.repositories;

import com.example.assignment_3_giorgos_and_hakon.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface FranchiseRepository extends JpaRepository<Franchise,Integer> {
}
