package com.example.assignment_3_giorgos_and_hakon.repositories;

import com.example.assignment_3_giorgos_and_hakon.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MovieRepository extends JpaRepository<Movie,Integer> {
}
