package com.example.assignment_3_giorgos_and_hakon.services.characterService;

import com.example.assignment_3_giorgos_and_hakon.models.Character;
import com.example.assignment_3_giorgos_and_hakon.models.Movie;
import com.example.assignment_3_giorgos_and_hakon.services.CRUDService;

import java.util.Collection;

public interface CharacterService extends CRUDService<Character, Integer> {

    Character update(Character entity);

    //Extra business logic
    String findByFull_name(String full_name);

    Collection<Movie> getMovies(int Character_id); // method that returns all the movies a character has played
}
