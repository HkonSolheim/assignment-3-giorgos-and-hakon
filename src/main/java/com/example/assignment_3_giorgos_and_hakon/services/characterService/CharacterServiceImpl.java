package com.example.assignment_3_giorgos_and_hakon.services.characterService;

import com.example.assignment_3_giorgos_and_hakon.models.Character;
import com.example.assignment_3_giorgos_and_hakon.models.Movie;
import com.example.assignment_3_giorgos_and_hakon.repositories.CharacterRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.Set;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;




    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findByID(int id) {
        return characterRepository.findById(id).get();
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteByID(Integer integer) {
        if(characterRepository.existsById(integer)){
            Character ch = characterRepository.findById(integer).get();
            ch.setMoviesPlayed(null);
            characterRepository.delete(ch);
        }
    }

    @Override
    public void delete(Character entity) {

    }

    @Override
    public String findByFull_name(String name) {

        return characterRepository.findAllByName(name).toString();
    }

    @Override
    public Set<Movie> getMovies(int character_id) {              //Method for returning all the moves a character has played
        return  characterRepository.findById(character_id).get().getMoviesPlayed();
    }
}
