package com.example.assignment_3_giorgos_and_hakon.services.movieService;
import com.example.assignment_3_giorgos_and_hakon.models.Movie;
import com.example.assignment_3_giorgos_and_hakon.services.CRUDService;

public interface MovieService extends CRUDService<Movie, Integer> {

    void updateCharacters(int movieId, int[] charactersID);

}
