package com.example.assignment_3_giorgos_and_hakon.services.movieService;

import com.example.assignment_3_giorgos_and_hakon.models.Character;
import com.example.assignment_3_giorgos_and_hakon.models.Movie;
import com.example.assignment_3_giorgos_and_hakon.repositories.CharacterRepository;
import com.example.assignment_3_giorgos_and_hakon.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MovieServiceImp implements MovieService{


    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;

    public MovieServiceImp(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findByID(int id) {
        return movieRepository.findById(id).get();
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteByID(Integer integer) {

    }

    @Override
    public void delete(Movie entity) {

    }

    @Override
    public void updateCharacters(int movieId, int[] charactersID) {     //*updates characters in a movie*
        Movie movie = movieRepository.findById(movieId).get();      //for a given array of characters ID
        Set<Character> characterList = new HashSet<>();             //we take a movie and pass that array to set characters method

        for (int id: charactersID) {
            characterList.add(characterRepository.findById(id).get());
        }

        movie.setCharacters(characterList);
        movieRepository.save(movie);
    }
}
