package com.example.assignment_3_giorgos_and_hakon.services;


import java.util.Collection;

public interface CRUDService<T,ID> {
    //Generic CRUD

    T findByID(int id);

    Collection<T> findAll();

    T add(T entity);
    T update(T entity);

    void deleteByID(ID id);
    void delete(T entity);
}
