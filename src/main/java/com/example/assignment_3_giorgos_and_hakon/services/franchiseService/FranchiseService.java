package com.example.assignment_3_giorgos_and_hakon.services.franchiseService;
import com.example.assignment_3_giorgos_and_hakon.services.CRUDService;
import com.example.assignment_3_giorgos_and_hakon.models.Franchise;

    public interface FranchiseService extends CRUDService<Franchise, Integer> {

    }

