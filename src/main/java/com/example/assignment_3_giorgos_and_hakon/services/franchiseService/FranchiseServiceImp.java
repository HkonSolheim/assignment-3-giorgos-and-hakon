package com.example.assignment_3_giorgos_and_hakon.services.franchiseService;

import com.example.assignment_3_giorgos_and_hakon.models.Franchise;
import com.example.assignment_3_giorgos_and_hakon.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FranchiseServiceImp implements FranchiseService {

    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImp(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }


    @Override
    public Franchise findByID(int id) {
        return franchiseRepository.findById(id).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteByID(Integer integer) {

    }

    @Override
    public void delete(Franchise entity) {

    }
}