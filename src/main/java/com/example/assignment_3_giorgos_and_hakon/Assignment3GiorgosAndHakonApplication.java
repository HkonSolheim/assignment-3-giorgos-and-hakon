package com.example.assignment_3_giorgos_and_hakon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3GiorgosAndHakonApplication {

    public static void main(String[] args) {
        SpringApplication.run(Assignment3GiorgosAndHakonApplication.class, args);

    }
}
