package com.example.assignment_3_giorgos_and_hakon.controllers;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/v1/character") //base url
public class CharacterController {
    @GetMapping("{id}")
    public ResponseEntity<String> path(@PathVariable int id) {
        return ResponseEntity.ok().body(String.valueOf(id));
    }

    @GetMapping
    public ResponseEntity<String> query(@RequestParam String key) {
        return ResponseEntity.ok().body(key);
    }

    @GetMapping("header")
    public ResponseEntity<String> headers(@RequestHeader("User-Agent") String agent) {
        return ResponseEntity.ok().body(agent);
    }

    @GetMapping("body")
    public ResponseEntity<String> body(@RequestBody String value) {
        return ResponseEntity.ok().body(value);
    }
}
