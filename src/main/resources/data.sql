-- CHARACTER
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Tony Stark', 'Iron Man', 'Male', 'https://i.pinimg.com/originals/12/0b/a2/120ba23b6db09ec9652b259ac8554f48.jpg');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Peter Parker', 'Spiderman','Male','https://th.bing.com/th/id/R.d347e954f435f90115f786310d32e269?rik=8qWFedTYe5Be1Q&riu=http%3a%2f%2fpm1.narvii.com%2f7081%2fafe47372cd5d26a6b958f5cba64e7bdc585bf610r1-719-959v2_uhq.jpg&ehk=Nqd51TswP1J%2b82djxRC13s%2f5VQxk8fmBta7FJpxXTYY%3d&risl=&pid=ImgRaw&r=0');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Natasha Romanoff','Black Widow','Female','https://www.fortressofsolitude.co.za/wp-content/uploads/2020/10/Black-Widow-1.jpg');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Sam Witwicky','','Male','https://th.bing.com/th/id/R.c1405fa9bb3ad0192c0f3f35e14f5659?rik=OV0Lge7BlcSn6g&riu=http%3a%2f%2fwww.aceshowbiz.com%2fimages%2fstill%2ftransformers_rotf104.jpg&ehk=6gOkULSR%2fU8VhtWfgPcR6GHZETlFPgJWtKAhVbwja%2fw%3d&risl=&pid=ImgRaw&r=0');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Mikaela Banes','','Female','https://th.bing.com/th/id/R.ef8c0f535f76d27965ab6a712d6c5354?rik=haQcmdMDT3YHNQ&riu=http%3a%2f%2fimages5.fanpop.com%2fimage%2fpolls%2f1057000%2f1057475_1340264065250_full.jpg&ehk=hapOykpp30Mk7JqR3SSFTwroThDZiREimjxcJoAJbts%3d&risl=&pid=ImgRaw&r=0');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Legolas','Greenleaf','Male','https://static.wikia.nocookie.net/lotr/images/3/33/Legolas_-_in_Two_Towers.PNG/revision/latest?cb=20120916035151');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Gimli','Elf-friend','Male','https://th.bing.com/th/id/R.97f42309db427c761c7581f96a47b43f?rik=zzB5k00HJBKmXQ&riu=http%3a%2f%2fvignette1.wikia.nocookie.net%2flotr%2fimages%2fe%2fec%2fGimli_-_FOTR.png%2frevision%2flatest%3fcb%3d20121008105956&ehk=%2bWv0HwDhzzahbYYnWrRVL3poyAsdMQ52XU36Wi8NBus%3d&risl=&pid=ImgRaw&r=0');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Aragorn','Elessar','Male','https://yt3.ggpht.com/a/AGF-l79K2Y6jWcTUOml19IZdhEbkAVYv5OqDNUT2wQ=s900-c-k-c0xffffffff-no-rj-mo');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Galadriel','','Female','https://vignette.wikia.nocookie.net/pjmidearthfilms/images/d/da/Image-1425857805.jpg/revision/latest?cb=20150308233645');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Tauriel','','Female','https://vignette4.wikia.nocookie.net/lotrfanon/images/1/1d/Tauriel-2.jpg/revision/latest?cb=20140514215355');
INSERT INTO character ("name", "alias", "gender", "picture_url") VALUES ('Bilbo Baggins','','Male','https://vignette.wikia.nocookie.net/andy-spencer-fanfic-writer/images/d/df/Bilbo_baggins_Hobbit3.png/revision/latest?cb=20161012005312');

-- MOVIE
INSERT INTO movie ("title", "genre",release_year,"director","picture_url","trailer_link") VALUES ('Lord of The Rings: The Fellowship of the Ring','Fantasy', 2001, 'Peter Jackson','https://th.bing.com/th/id/R.84e406df9c9292eb20dea838d27d86e5?rik=agTpyv%2blL5K%2fOA&pid=ImgRaw&r=0','https://www.youtube.com/watch?v=V75dMMIW2B4');
INSERT INTO movie ("title", "genre",release_year,"director","picture_url","trailer_link") VALUES ('The Hobbit: An Unexpected Journey','Fantasy', 2014, 'Peter Jackson','https://th.bing.com/th/id/OIP.j69bSsx0m-ISRlnIgeE1tQHaLH?pid=ImgDet&rs=1','https://www.youtube.com/watch?v=SDnYMbYB-nU');
INSERT INTO movie ("title", "genre",release_year,"director","picture_url","trailer_link") VALUES ('The Avengers','Fantasy', 2012, 'Joss Wheldon','https://th.bing.com/th/id/R.dae0611f00c3ce76d941f707c586d1d0?rik=wjHybzGkIfGfyg&pid=ImgRaw&r=0','https://www.youtube.com/watch?v=eOrNdBpGMv8');
INSERT INTO movie ("title", "genre",release_year,"director","picture_url","trailer_link") VALUES ('Transformers','Sci-fi', 2007 , 'Michael Bay','https://upload.wikimedia.org/wikipedia/en/6/66/Transformers07.jpg','https://www.youtube.com/watch?v=dxQxgAfNzyE');

-- FRANCHISE
INSERT INTO franchise ("name", "description") VALUES ('Lord of The Rings','Epic fantasy adventure film based on the books by J.R.R Tolkien.'); -- 1
INSERT INTO franchise ("name", "description") VALUES ('The Avengers','Action and Adventures from the Marvel universe.'); -- 1
INSERT INTO franchise ("name", "description") VALUES ('Transformers','Futuristic and Space themed action'); -- 1

